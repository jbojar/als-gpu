#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <curand.h>
#include <cstdlib>
#include <iostream>
#include <thrust/sort.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sequence.h>
#include <time.h>
#include "timer.h"
#include "utils.h"

#include "MatrixMult.h"

__global__ void __dmv(float *a, int nrows, int ncols, float *b, float *c);
__global__ void __dmvt(float *a, int nrows, int ncols, float *b, float *c);
__global__ void __dmv0(float *a, int nrows, int ncols, int tstep, float *b, float *c);
int dmv(float *a, int nrows, int ncols, float *b, float *c, int trans);

MatrixMult::MatrixMult(size_t nr_rows_A, size_t nr_cols_A): nr_rows_A(nr_rows_A), nr_cols_A(nr_cols_A), d_A(nr_rows_A * nr_cols_A, 0) {
  GPU_fill_rand(thrust::raw_pointer_cast(&d_A[0]), nr_rows_A, nr_cols_A);
}

void MatrixMult::multiply(thrust::host_vector<float> h_B, int h_indices[], size_t count) {
  thrust::device_vector<float> d_B(nr_cols_A, 0);
  d_B = h_B;
  multiply(d_B, h_indices, count);
}

void MatrixMult::multiply(thrust::device_vector<float> d_B, int h_indices[], size_t count) {
  std::lock_guard<std::mutex> lck(mtx);
  thrust::device_vector<float> d_C(nr_rows_A, 0);

  thrust::fill(d_C.begin(), d_C.end(), 0);
  dmv(thrust::raw_pointer_cast(&d_A[0]), nr_rows_A, nr_cols_A, thrust::raw_pointer_cast(&d_B[0]), thrust::raw_pointer_cast(&d_C[0]), 0);

  thrust::device_vector<int> d_indices(nr_rows_A, 0);

  thrust::sequence(d_indices.begin(), d_indices.end());
  thrust::sort_by_key(d_C.begin(), d_C.end(), d_indices.begin());

  cudaMemcpy(h_indices, thrust::raw_pointer_cast(&d_indices[0]), count * sizeof(int), cudaMemcpyDeviceToHost);
}

__global__ void __dmv(float *a, int nrows, int ncols, float *b, float *c) {
  for (int tx = threadIdx.x + blockDim.x * blockIdx.x; tx < nrows; tx += blockDim.x * gridDim.x) {
    float accum = 0.0f;
    for (int ty = threadIdx.y + blockDim.y * blockIdx.y; ty < ncols; ty += blockDim.y * gridDim.y) {
      accum += a[tx+nrows*ty] * b[ty];
    }
    atomicAdd(&c[tx], accum);
  }
}

#if __CUDA_ARCH__ > 200

__global__ void __dmvt(float *a, int nrows, int ncols, float *b, float *c) {
  for (int ty = threadIdx.y + blockDim.y * blockIdx.y; ty < ncols; ty += blockDim.y * gridDim.y) {
    float accum = 0.0f;
    for (int tx = threadIdx.x + blockDim.x * blockIdx.x; tx < nrows; tx += blockDim.x * gridDim.x) {
      accum += a[tx+nrows*ty] * b[tx];
    }
    for (int i = 1; i < blockDim.x; i *= 2) {
      float tmp = __shfl_down(accum, i);
      if (threadIdx.x + i < blockDim.x) accum += tmp;
    }
    if (threadIdx.x == 0) {
      atomicAdd(&c[ty], accum);
    }
  }
}
#else
__global__ void __dmvt(float *a, int nrows, int ncols, float *b, float *c) {
  for (int ty = threadIdx.y + blockDim.y * blockIdx.y; ty < ncols; ty += blockDim.y * gridDim.y) {
    float accum = 0.0f;
    for (int tx = threadIdx.x + blockDim.x * blockIdx.x; tx < nrows; tx += blockDim.x * gridDim.x) {
      accum += a[tx+nrows*ty] * b[tx];
    }
    atomicAdd(&c[ty], accum);
  }
}

#endif

__global__ void __dmv0(float *a, int nrows, int ncols, int tstep, float *b, float *c) {
  float accum = 0.0f;
  int tx = threadIdx.x + blockDim.x * blockIdx.x;
  if (tx < tstep) {
    for (; tx < nrows*ncols; tx += tstep) {
      int icol = tx / nrows;
      accum += a[tx] * b[icol];
    }
    int irow = tx % nrows;
    atomicAdd(&c[irow], accum);
  }
}

int dmv(float *a, int nrows, int ncols, float *b, float *c, int trans) {
  if (trans == 1) {
    int ntx = min(32, nrows);
    int nty = min(32, ncols);
    int nbx = min(256, 1 + nrows/ntx/8);
    int nby = min(256, 1 + ncols/nty/2);
    dim3 blockdims(ntx,nty,1);
    dim3 griddims(nbx,nby,1);
    __dmvt<<<griddims,blockdims>>>(a, nrows, ncols, b, c);
  } else {
    int ntx = min(1024, nrows*ncols);
    int nbx = max(1+(nrows-1)/ntx, nrows*ncols/ntx/32);
    int tstep = (ntx*nbx/nrows)*nrows;
    __dmv0<<<nbx,ntx>>>(a, nrows, ncols, tstep, b, c);
  }
  cudaDeviceSynchronize();
  cudaError_t err = cudaGetLastError();
  return err;
}
