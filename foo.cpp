#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <curand.h>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <thrust/sort.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sequence.h>
#include <time.h>
#include <microhttpd.h>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <vector>
#include "timer.h"
#include "utils.h"

#include "MatrixMult.h"

#define PORT 8888

using namespace std;

void random_fill(float *v, int length) {

  while(length-- > 0) {
    v[length] = ((float)rand()) / RAND_MAX;
  }
}

template<typename T> void print_matrix(const T *A, int nr_rows_A, int nr_cols_A) {

  for(int i = 0; i < nr_rows_A; ++i){
    for(int j = 0; j < nr_cols_A; ++j){
      std::cout << A[j * nr_rows_A + i] << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;

}

int answer_to_connection (void *cls, struct MHD_Connection *connection,
                          const char *url,
                          const char *method, const char *version,
                          const char *upload_data,
                          size_t *upload_data_size, void **con_cls) {
    static int aptr;
    MatrixMult* ctx = static_cast<MatrixMult*>(cls);
    struct MHD_Response *response;
    int ret;

    const string multiply_url_prefix = "/multiply/";

    if (0 != strcmp (method, "GET"))
        return MHD_NO;              /* unexpected method */

    if (!starts_with(url, multiply_url_prefix.c_str())) {
        return MHD_NO;
    }

    if (&aptr != *con_cls) {
        /* do never respond on first call */
        *con_cls = &aptr;
        return MHD_YES;
    }
    *con_cls = NULL;                  /* reset when done */

    string data = string(url + multiply_url_prefix.length());

    boost::char_separator<char> sep(",");
    boost::tokenizer< boost::char_separator<char> > tokens(data, sep);
    vector<float> user_features;
    BOOST_FOREACH (const string& t, tokens) {
        user_features.push_back(std::stof(t));
    }

    thrust::host_vector<float> h_B(user_features);

    const int limit = 20;
    int h_indices[limit];
    ctx->multiply(h_B, h_indices, limit);

    std::ostringstream ss;
    for (int i = 0; i < limit; i++) {
      ss << h_indices[i] << ",";
    }
    string resp_body = ss.str();

    response = MHD_create_response_from_buffer (strlen (resp_body.c_str()),
                                                (void *) resp_body.c_str(),
                                                MHD_RESPMEM_MUST_COPY);
    ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
    MHD_destroy_response (response);

    return ret;
}


int main(int argc, char *argv[]) {

  srand(time(NULL));

  MatrixMult mm(3500000, 80);

  struct MHD_Daemon *daemon;
  daemon = MHD_start_daemon (MHD_USE_SELECT_INTERNALLY | MHD_USE_POLL, PORT, NULL, NULL, &answer_to_connection, &mm, MHD_OPTION_THREAD_POOL_SIZE, 16, MHD_OPTION_END);

  if (NULL == daemon) {
      printf("Error while creating MHD Daemon\n");
      return 1;
  }
  printf("DONE\n");

  printf("Waiting for requests...\n");

  getchar ();
  MHD_stop_daemon (daemon);

  return 0;

  thrust::host_vector<float> h_B(80);

  // Multiply A and B on GPU

  int N = 10;

  {
    float time_sum = 0.0f;
    for (int i = 0; i < N; ++i) {

      random_fill(thrust::raw_pointer_cast(&h_B[0]), 80);

      GpuTimer timer;
      timer.Start();

      int h_indices[20];
      mm.multiply(h_B, h_indices, 20);

      timer.Stop();

      time_sum += timer.Elapsed();

      std::cout.precision(6);

      std::cout << "Time spent all:  " << std::fixed << timer.Elapsed() << std::endl;
      print_matrix(h_indices, 1, 20);
    }

    std::cout << "Average time: " << std::fixed << time_sum / N << std::endl;
  }

  return 0;
}
