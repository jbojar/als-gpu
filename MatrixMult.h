#ifndef MATRIX_MULT_H
#define MATRIX_MULT_H

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <mutex>

class MatrixMult {
public:
  MatrixMult(size_t nr_rows_A, size_t nr_cols_A);

  void multiply(thrust::host_vector<float> h_B, int h_indices[], size_t count);

  void multiply(thrust::device_vector<float> d_B, int h_indices[], size_t count);

private:
  size_t nr_rows_A, nr_cols_A;
  thrust::device_vector<float> d_A;
  std::mutex mtx;
};

#endif
