#ifndef UTILS_H
#define UTILS_H
#include <stdio.h>
#include <stdbool.h>

void GPU_fill_rand(float *A, int nr_rows_A, int nr_cols_A);

bool starts_with(const char *str, const char *pre);

char* get_user_home();

#endif
