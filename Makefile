NVCC_OPTS=--std=c++11 -O3 -m64 -arch=compute_30

all: foo

foo: foo.o heap.o MatrixMult.o utils.o
	nvcc foo.o heap.o MatrixMult.o utils.o -lcurand -lcublas -lmicrohttpd -o foo $(NVCC_OPTS)

foo.o: foo.cpp
	nvcc -c foo.cpp -o foo.o $(NVCC_OPTS)

heap.o: heap.c heap.h
	nvcc -c heap.c -o heap.o $(NVCC_OPTS)

MatrixMult.o: MatrixMult.cu MatrixMult.h
	nvcc -c MatrixMult.cu -o MatrixMult.o $(NVCC_OPTS)

utils.o: utils.cpp utils.h
	nvcc -c utils.cpp -o utils.o $(NVCC_OPTS)

clean:
	rm -fv *.o
	rm -fv foo
