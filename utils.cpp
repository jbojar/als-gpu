#include <curand.h>
#include <time.h>
#include "utils.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>

bool starts_with(const char *str, const char *pre) {
    size_t lenpre = strlen(pre),
    lenstr = strlen(str);
    return lenstr < lenpre ? false : strncmp(pre, str, lenpre) == 0;
}

char* get_user_home() {
    size_t bufsize;

    if ((bufsize = sysconf(_SC_GETPW_R_SIZE_MAX)) == -1)
        abort();

    char buffer[bufsize];
    struct passwd pwd, *result = NULL;
    if (getpwuid_r(getuid(), &pwd, buffer, bufsize, &result) != 0 || !result) {
        return NULL;
    }

    size_t  home_dir_len = strlen(pwd.pw_dir) + 1;
    char* home_dir = (char*)malloc(home_dir_len);
    strncpy(home_dir, pwd.pw_dir, home_dir_len);

    return home_dir;
}

void GPU_fill_rand(float *A, int nr_rows_A, int nr_cols_A) {
  // Create a pseudo-random number generator
  curandGenerator_t prng;
  curandCreateGenerator(&prng, CURAND_RNG_PSEUDO_DEFAULT);

  // Set the seed for the random number generator using the system clock
  curandSetPseudoRandomGeneratorSeed(prng, (unsigned long long) clock());

  // Fill the array with random numbers on the device
  curandGenerateUniform(prng, A, nr_rows_A * nr_cols_A);
}
